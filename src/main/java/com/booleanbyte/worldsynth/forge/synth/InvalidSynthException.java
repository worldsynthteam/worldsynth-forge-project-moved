package com.booleanbyte.worldsynth.forge.synth;

import com.booleanbyte.worldsynth.common.Synth;

public class InvalidSynthException extends Exception {
	private static final long serialVersionUID = -6667918578298664070L;
	
	private final Synth synth;
	
	InvalidSynthException(String message, Synth synth) {
		super(message);
		this.synth = synth;
	}
	
	public Synth getSynth() {
		return synth;
	}
}
