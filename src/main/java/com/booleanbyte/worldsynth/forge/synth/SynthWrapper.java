package com.booleanbyte.worldsynth.forge.synth;

import java.util.Hashtable;

import com.booleanbyte.worldsynth.common.Synth;
import com.booleanbyte.worldsynth.module.AbstractModule;

public class SynthWrapper {
	
	private final Synth synth;
	
	private final Hashtable<String, AbstractModule> synthParameters = new Hashtable<String, AbstractModule>();
	private final AbstractModule forgeExporterModule;
	
	public SynthWrapper(Synth synth) throws InvalidSynthException {
		this.synth = synth;
		
		//TODO search for a forge exporter module
		forgeExporterModule = null;
		if(forgeExporterModule == null) {
			throw new InvalidSynthException("Invalid synth, has no forge export module", synth);
		}
		
		//TODO search for all parameter modules and add them to parameters table
	}
	
	public Synth getSynth() {
		return synth;
	}
}
