package com.booleanbyte.worldsynth.forge.proxy;

import java.io.File;

import com.booleanbyte.worldsynth.common.WorldSynthCore;
import com.booleanbyte.worldsynth.forge.WorldSynth;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy {

	public void preInit(FMLPreInitializationEvent event) {
		WorldSynth.LOGGER.info("Initializing WorldSynth Engine");
		
		//Get the root directory of the minecraft instance
		File mcDir = Loader.instance().getConfigDir().getParentFile();
		
		//Get the worldsynth directory for worldsynth configs and other 
		File worldSynthDir = new File(mcDir, "worldsynth");
		if(mcDir.exists() && !worldSynthDir.exists()) {
			worldSynthDir.mkdir();
		}
		
		//Get the worldsynth directory for worldsynth configs and other 
		File synthDir = new File(worldSynthDir, "synths");
		if(worldSynthDir.exists() && !synthDir.exists()) {
			synthDir.mkdir();
		}
		
		//Initialize the worldsynth engine
		new WorldSynthCore(worldSynthDir);
		
		WorldSynth.LOGGER.info("Done initializing WorldSynth Engine");
	}
}
