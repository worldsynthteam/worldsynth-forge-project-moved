package com.booleanbyte.worldsynth.forge;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.booleanbyte.worldsynth.common.Synth;
import com.booleanbyte.worldsynth.common.io.ProjectReader;
import com.booleanbyte.worldsynth.forge.proxy.CommonProxy;
import com.booleanbyte.worldsynth.forge.world.WorldTypeWorldSynth;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = WorldSynth.MOD_ID, acceptedMinecraftVersions = "[1.12.2]", useMetadata = true)
public class WorldSynth {
	
	public static final String MOD_ID = "worldsynthforge";
	
	public static final Logger LOGGER = LogManager.getLogger("worldsynthforge");

	@Instance
	public static WorldSynth instance;

	@SidedProxy(clientSide = "com.booleanbyte.worldsynth.forge.proxy.ClientProxy", serverSide = "com.booleanbyte.worldsynth.forge.proxy.CommonProxy")
	public static CommonProxy proxy;

	@EventHandler
	public static void preInit(FMLPreInitializationEvent event) {
		proxy.preInit(event);
		
		LOGGER.info("Preparing to import worldsynths");
		File synthsDir = new File(Loader.instance().getConfigDir().getParentFile(), "worldsynth/synths/");
		if(synthsDir.isDirectory()) {
			for(Synth s: getSynths(synthsDir)) {
				LOGGER.info("Importing \"" + s.getName() + "\"");
				new WorldTypeWorldSynth(s);
			}
		}
	}
	
	private static ArrayList<Synth> getSynths(File synthsDir) {
		ArrayList<Synth> synths = new ArrayList<Synth>();
		
		for(File f: synthsDir.listFiles()) {
			if(!f.getName().endsWith(".wsynth")) {
				continue;
			}
			
			synths.add(ProjectReader.readSynthFromFile(f));
		}
		
		return synths;
	}
}
