package com.booleanbyte.worldsynth.forge.world.gen;

import java.util.List;

import com.booleanbyte.worldsynth.common.Synth;

import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome.SpawnListEntry;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.IChunkGenerator;

public class ChunkGeneratorWorldSynth implements IChunkGenerator {

	private final Synth generatorSynth;
	
	public ChunkGeneratorWorldSynth(World worldIn, long seed, Synth synth, String generatorOptions) {
		generatorSynth = synth;
	}
	
	@Override
	public Chunk generateChunk(int x, int z) {
		return null;
	}

	@Override
	public void populate(int x, int z) {

	}

	@Override
	public boolean generateStructures(Chunk chunkIn, int x, int z) {
		return false;
	}

	@Override
	public List<SpawnListEntry> getPossibleCreatures(EnumCreatureType creatureType, BlockPos pos) {
		return null;
	}

	@Override
	public BlockPos getNearestStructurePos(World worldIn, String structureName, BlockPos position, boolean findUnexplored) {
		return null;
	}

	@Override
	public void recreateStructures(Chunk chunkIn, int x, int z) {

	}

	@Override
	public boolean isInsideStructure(World worldIn, String structureName, BlockPos pos) {
		return false;
	}

}
