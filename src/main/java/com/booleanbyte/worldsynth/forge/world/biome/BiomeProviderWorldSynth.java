package com.booleanbyte.worldsynth.forge.world.biome;

import com.booleanbyte.worldsynth.common.Synth;

import net.minecraft.world.biome.BiomeProvider;

public class BiomeProviderWorldSynth extends BiomeProvider {
	
	private final Synth generatorSynth;
	
	public BiomeProviderWorldSynth(Synth synth) {
		generatorSynth = synth;
	}
}
