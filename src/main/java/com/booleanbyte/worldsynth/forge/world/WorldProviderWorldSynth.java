package com.booleanbyte.worldsynth.forge.world;

import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldProvider;

public class WorldProviderWorldSynth extends WorldProvider {
	private final String name = "WorldSynth";
	private final String suffix = "";
	private final int id = 1000;
	private final Class <? extends WorldProvider> provider = WorldProviderWorldSynth.class;
	private final boolean keepLoaded = false;
	
	private final DimensionType dimensionType = DimensionType.register(name, suffix, id, provider, keepLoaded);
	
	@Override
	public DimensionType getDimensionType() {
		return dimensionType;
	}

}
