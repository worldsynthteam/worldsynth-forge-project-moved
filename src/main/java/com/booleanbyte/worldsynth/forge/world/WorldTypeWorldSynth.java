package com.booleanbyte.worldsynth.forge.world;

import com.booleanbyte.worldsynth.common.Synth;
import com.booleanbyte.worldsynth.forge.client.gui.customization.GuiCustomizationWorldSynthScreen;
import com.booleanbyte.worldsynth.forge.world.biome.BiomeProviderWorldSynth;
import com.booleanbyte.worldsynth.forge.world.gen.ChunkGeneratorWorldSynth;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiCreateWorld;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class WorldTypeWorldSynth extends WorldType {
	
	private final Synth generatorSynth;
	
	public WorldTypeWorldSynth(Synth synth) {
		super(synth.getName().substring(0, Math.min(15, synth.getName().length()-1)));
		generatorSynth = synth;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public String getTranslationKey() {
		return getName();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public String getInfoTranslationKey() {
		return "A worldSynth world type";
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasInfoNotice() {
		return true;
	}
	
	@Override
	public boolean isCustomizable() {
		return false;
	}
	
	@Override
	public IChunkGenerator getChunkGenerator(World world, String generatorOptions) {
		return new ChunkGeneratorWorldSynth(world, world.getSeed(), generatorSynth, generatorOptions);
	}
	
	@Override
	public BiomeProvider getBiomeProvider(World world) {
		return new BiomeProviderWorldSynth(generatorSynth);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void onCustomizeButton(Minecraft mc, GuiCreateWorld guiCreateWorld) {
		mc.displayGuiScreen(new GuiCustomizationWorldSynthScreen(guiCreateWorld, guiCreateWorld.chunkProviderSettingsJson));
	}
}
