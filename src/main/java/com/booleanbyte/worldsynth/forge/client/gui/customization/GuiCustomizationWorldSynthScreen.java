package com.booleanbyte.worldsynth.forge.client.gui.customization;

import net.minecraft.client.gui.GuiCreateWorld;
import net.minecraft.client.gui.GuiPageButtonList;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.world.gen.ChunkGeneratorSettings;

public class GuiCustomizationWorldSynthScreen extends GuiScreen {
	private final GuiCreateWorld parent;
	
	private ChunkGeneratorSettings.Factory settings;
	
	public GuiCustomizationWorldSynthScreen(GuiScreen parentIn, String generatorSynthSettingsJson) {
		this.parent = (GuiCreateWorld)parentIn;
//        this.loadValues(generatorSynthSettingsJson);
	}
	
	@Override
	public void initGui() {
		super.initGui();
	}

}